//
//  CHADAppDelegate.h
//  CHAD
//
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 
 ======== ============ ========= ================================================================
 
 */

#import <UIKit/UIKit.h>

@class chadViewController;

@interface CHADAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	chadViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet chadViewController *viewController;

@end

