//
//  MenuView.h
//  CHAD
//
//  Created by Robin Mahony on 11-02-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 04/7/11  Robin Mahony 2.0		Added exitButton, and some graphics
 ======== ============ ========= ================================================================
 
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class chadViewController;

@interface MenuView : UIView {
	IBOutlet chadViewController *mainController;
	IBOutlet UIButton *startButton;
	IBOutlet UIButton *exitButton;

}

@property (nonatomic, retain) IBOutlet chadViewController *mainController;
@property (nonatomic, retain) IBOutlet UIButton *startButton;
@property (nonatomic, retain) IBOutlet UIButton *exitButton;

-(IBAction)startButton;
-(IBAction)exitButton;

@end
