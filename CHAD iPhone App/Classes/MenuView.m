    //
//  MenuView.m
//  CHAD
//
//  Created by Robin Mahony on 11-02-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 03/13/11 Robin Mahony 1.1		added alertview alert for notifying user to connect to the internet
 04/7/11  Robin Mahony 2.0		Added exitButton function to handle exiting app
 ======== ============ ========= ================================================================
 
 */

#import "MenuView.h"

#import "chadViewController.h"
#import "CustomMoviePlayerViewController.h"

@implementation MenuView

@synthesize mainController, startButton;

-(void)awakeFromNib{
	NSLog(@"entered awake from nib, in MenuView");

		
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	
	if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
		return YES;
	
	return NO;
}

-(IBAction)startButton{
	
	
	if( [mainController connectedToNetwork] )
	{
		//[mainController loadMoviePlayer];
		[mainController loadActiveView];
		
	}
	
	else 
	{
		UIAlertView *alert = nil;
		alert = [[UIAlertView alloc] 
				 initWithTitle:[NSString stringWithFormat:@"No internet connection"]
				 message:@"Please connect to the internet, then try again."
				 delegate:self 
				 cancelButtonTitle:@"Back" 
				 otherButtonTitles:nil];
		alert.tag = 1;
		[alert show];
		[alert release];
	}
	



}

-(IBAction)exitButton{
	
	exit(0);
	
	
}
/*
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}*/


- (void)dealloc {
    [super dealloc];
}


@end
