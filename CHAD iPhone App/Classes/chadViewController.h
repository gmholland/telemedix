//
//  chadViewController.h
//  CHAD
//
//  Created by Robin Mahony on 11-02-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 04/7/11  Robin Mahony 2.0		Added "backToMenuView" function, and removed "startApp" function since
								it is not needed.
 ======== ============ ========= ================================================================
 
 */

#import <UIKit/UIKit.h>
#import "CustomMoviePlayerViewController.h" 

@class MenuView, ActiveView, CHADAppDelegate;


@interface chadViewController : UIViewController {
	IBOutlet MenuView *menuView;
	IBOutlet ActiveView *activeView;
	IBOutlet CHADAppDelegate *myDelegate;
	
	CustomMoviePlayerViewController *moviePlayer;

}

@property (nonatomic, retain) IBOutlet MenuView *menuView;
@property (nonatomic, retain) IBOutlet ActiveView *activeView;
@property (nonatomic, retain) IBOutlet CHADAppDelegate *myDelegate;
@property (nonatomic, retain) CustomMoviePlayerViewController *moviePlayer;

//-(void)startApp;
-(void)loadMoviePlayer;
-(void)loadActiveView;
-(void)backToMenuView;
//-(int)checkWIFI;
-(BOOL)connectedToNetwork;

-(void)setDisconnectFlag;

@end
