//
//  CustomMoviePlayerViewController.m
//
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 04/7/11  Robin Mahony 2.0		Added "unloadPlayer", which is used by chadViewController to remove
								the movie player when we want to go back to the main menu
 ======== ============ ========= ================================================================
 
 */

#import "CustomMoviePlayerViewController.h"

#pragma mark -
#pragma mark Compiler Directives & Static Variables

@implementation CustomMoviePlayerViewController

@synthesize mp;

/*---------------------------------------------------------------------------
 
 ---------------------------------------------------------------------------*/

-(void)unloadPlayer{
	NSLog(@"unloading movie player");
	[mp stop];

	// Register that the load state changed (movie is ready)
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePreloadDidFinish:) 
												 name:MPMoviePlayerPlaybackDidFinishNotification 
											   object:nil];
}


/*---------------------------------------------------------------------------
* 
*--------------------------------------------------------------------------*/
- (id)initWithPath:(NSString *)moviePath
{
	// Initialize and create movie URL
  if (self = [super init])
  {		
	 // movieURL = [NSURL URLWithString:@"http://telemedix.dyndns.org:8080/?action=stream"];
	  movieURL = [NSURL URLWithString:@"http://142.58.172.188:8080/?action=stream"];
	 //movieURL = [NSURL URLWithString:@"http://70.68.51.219/streaming/stream_cell_16x9_150k.m3u8"];
	  movieURL = [NSURL URLWithString:@"http://dl.dropbox.com/u/3992930/3-01%20Alabama%20Motel%20Room.mp4"];
	 // movieURL = [NSURL fileURLWithPath:moviePath];
	  
	 // NSLog(@"%@",[[NSURL fileURLWithPath:moviePath] absoluteString]);
	[movieURL retain];
  }
	return self;
}

/*---------------------------------------------------------------------------
* For 3.2 and 4.x devices
* For 3.1.x devices see moviePreloadDidFinish:
*--------------------------------------------------------------------------*/
- (void) moviePlayerLoadStateChanged:(NSNotification*)notification 
{
	// Unless state is unknown, start playback
	if ([mp loadState] != MPMovieLoadStateUnknown)
  {
  	// Remove observer
    [[NSNotificationCenter 	defaultCenter] 
    												removeObserver:self
                         		name:MPMoviePlayerLoadStateDidChangeNotification 
                         		object:nil];

    // When tapping movie, status bar will appear, it shows up
    // in portrait mode by default. Set orientation to landscape
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];

		// Rotate the view for landscape playback
		//[[self view] setBounds:CGRectMake(0, 0, 480, 320)];
		//[[self view] setCenter:CGPointMake(160, 240)];
		//[[self view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)];

		// Set frame of movieplayer
		[[mp view] setFrame:CGRectMake(0, 0, 480, 320)];
    
    // Add movie player as subview
	  [[self view] addSubview:[mp view]];
		
		// Play the movie
	  NSLog(@"playing movie in moviePlayerLoadStateChanged");
	   //mp.useApplicationAudioSession = NO;

	  [mp play];
	
	  //[mp stop];
	  //NSLog(@"pausing movie");
	  //[mp pause];
	}
}

/*---------------------------------------------------------------------------
* For 3.1.x devices
* For 3.2 and 4.x see moviePlayerLoadStateChanged: 
*--------------------------------------------------------------------------*/
- (void) moviePreloadDidFinish:(NSNotification*)notification 
{
	// Remove observer
	[[NSNotificationCenter 	defaultCenter] 
													removeObserver:self
                        	name:MPMoviePlayerContentPreloadDidFinishNotification
                        	object:nil];

	
	NSLog(@"playing movie in moviePreloadDidFinish");
	// Play the movie
 	[mp play];
	
	//NSLog(@"pausing movie");
	//[mp pause];
}

/*---------------------------------------------------------------------------
* 
*--------------------------------------------------------------------------*/
- (void) moviePlayBackDidFinish:(NSNotification*)notification 
{    
	
	NSLog(@"movie playback finished");
  [[UIApplication sharedApplication] setStatusBarHidden:YES];

 	// Remove observer
  [[NSNotificationCenter 	defaultCenter] 
  												removeObserver:self
  		                   	name:MPMoviePlayerPlaybackDidFinishNotification 
      		               	object:nil];

	[self dismissModalViewControllerAnimated:YES];	
}

/*---------------------------------------------------------------------------
*
*--------------------------------------------------------------------------*/
- (void) readyPlayer
{
	NSLog(@"entered readyPlayer");
 	mp =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
	
  if ([mp respondsToSelector:@selector(loadState)]) 
  {
  	// Set movie player layout
  	[mp setControlStyle:MPMovieControlStyleNone];
    [mp setFullscreen:YES];

		// May help to reduce latency
		[mp prepareToPlay];

		// Register that the load state changed (movie is ready)
		[[NSNotificationCenter defaultCenter] addObserver:self 
                       selector:@selector(moviePlayerLoadStateChanged:) 
                       name:MPMoviePlayerLoadStateDidChangeNotification 
                       object:nil];
	}  
  else
  {
    // Register to receive a notification when the movie is in memory and ready to play.
    [[NSNotificationCenter defaultCenter] addObserver:self 
                         selector:@selector(moviePreloadDidFinish:) 
                         name:MPMoviePlayerContentPreloadDidFinishNotification 
                         object:nil];
  }

  // Register to receive a notification when the movie has finished playing. 
  [[NSNotificationCenter defaultCenter] addObserver:self 
                        selector:@selector(moviePlayBackDidFinish:) 
                        name:MPMoviePlayerPlaybackDidFinishNotification 
                        object:nil];
}

/*
 Ensure movie player auto rotates for landscape orientations
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	
	if((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation == UIInterfaceOrientationLandscapeRight))
		return YES;
	
	return NO;
}

/*---------------------------------------------------------------------------
* 
*--------------------------------------------------------------------------*/
- (void) loadView
{
  [self setView:[[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease]];
	[[self view] setBackgroundColor:[UIColor blackColor]];
}

/*---------------------------------------------------------------------------
*  
*--------------------------------------------------------------------------*/
- (void)dealloc 
{
	[mp release];
  [movieURL release];
	[super dealloc];
}

@end
