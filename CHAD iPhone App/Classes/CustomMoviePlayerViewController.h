//
//  CustomMoviePlayerViewController.h
//

//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 04/7/11  Robin Mahony 2.0		Added "unloadPlayer" function
 ======== ============ ========= ================================================================
 
 */

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>


@interface CustomMoviePlayerViewController : UIViewController 
{
  MPMoviePlayerController *mp;
  NSURL 									*movieURL;
}

@property (nonatomic, retain) MPMoviePlayerController *mp;

- (id)initWithPath:(NSString *)moviePath;
- (void)readyPlayer;
- (void)unloadPlayer;

@end
