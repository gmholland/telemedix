//
//  ActiveView.h
//  CHAD
//
//  Created by Robin Mahony on 11-02-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version. testing
 02/24/11 Robin Mahony 1.1		added new buttons and button methods
 03/13/11 Robin Mahony 2.0		added socket control
 03/23/11 Robin Mahony 3.0		removed "stop" button, added all audio buttons and methods
 04/7/11  Robin Mahony 3.1		Added "exitFlag" and "disconnectFlag", "goToMainMenu" function and 
							    "onSocketDidDisconnect" function
 ======== ============ ========= ================================================================
 
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioSession.h>
#import <AudioToolbox/AudioServices.h>
#import "AsyncSocket.h"



@class chadViewController;

@interface ActiveView : UIView  
<AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
	
	IBOutlet UIWebView *webView;
	
	IBOutlet UIButton *leftRobotButton;
	IBOutlet UIButton *downRobotButton;
	IBOutlet UIButton *rightRobotButton;
	IBOutlet UIButton *upRobotButton;
	IBOutlet UIButton *leftCamButton;
	IBOutlet UIButton *downCamButton;
	IBOutlet UIButton *rightCamButton;
	IBOutlet UIButton *upCamButton;
	IBOutlet UIButton *centerCamButton;
	
	IBOutlet UIButton *backButton;
	
	IBOutlet UIButton *refreshWebview;
	
	IBOutlet chadViewController *myParent; //gives ActiveView access to its parent viewController.

	
	int controlString;
	
	int exitFlag; //used to determine if ActiveView has been exited already, so won't try to exit twice
	//ie. press start, then exit to MenuView before socket connection check is done.  checked in alertView handler
	int disconnectFlag; //used so that alertView pop up wont occur twice, when it shouldnt.
						//ie. since socket disconnect method is called when a socket fails to connect
						//and whenever activeView quits to MenuView, AlertView was popping up twice
	
	int tactileOption;
	
	int contCamFlag;
	
	NSTimer *myTimer;
	
	AsyncSocket *testSocket;
	AsyncSocket *testAudioSocket;

	AVAudioRecorder *audioRecorder;
	AVAudioPlayer *audioPlayer;
	UIButton *playButton;
	UIButton *recordButton;
	//UIButton *stopButton;
}



@property (nonatomic, retain) IBOutlet UIButton *upCamButton;
@property (nonatomic, retain) IBOutlet UIButton *downCamButton;
@property (nonatomic, retain) IBOutlet UIButton *leftCamButton;
@property (nonatomic, retain) IBOutlet UIButton *rightCamButton;
@property (nonatomic, retain) IBOutlet UIButton *centerCamButton;

@property (nonatomic, retain) IBOutlet UIButton *upRobotButton;
@property (nonatomic, retain) IBOutlet UIButton *downRobotButton;
@property (nonatomic, retain) IBOutlet UIButton *leftRobotButton;
@property (nonatomic, retain) IBOutlet UIButton *rightRobotButton;

@property (nonatomic, retain) IBOutlet UIButton *backButton;

//@property (nonatomic, retain) IBOutlet chadViewController *videoController;

@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, retain) IBOutlet UIButton *recordButton;
//@property (nonatomic, retain) IBOutlet UIButton *stopButton;

@property (nonatomic, retain) IBOutlet UIWebView *webView;

@property (nonatomic, retain) IBOutlet UIButton *refreshWebview;

@property int exitFlag;
@property int disconnectFlag;
@property int tactileOption;

@property int contCamFlag;

-(void)connectToSocket;
-(void)connectToAudioSocket;
-(void)disconnectSocket;
-(void)sendToSocket;
-(void)onSocketDidDisconnect:(AsyncSocket *)sock;
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port;
-(IBAction)goToMainMenu;

-(IBAction) recordAudio;
-(IBAction) playAudio;
-(IBAction) stop;


-(IBAction)upCamTouchDown;
-(IBAction)downCamTouchDown;
-(IBAction)leftCamTouchDown;
-(IBAction)rightCamTouchDown;
-(IBAction)centerCamTouchDown;

-(IBAction)upRobotTouchDown;
-(IBAction)downRobotTouchDown;
-(IBAction)leftRobotTouchDown;
-(IBAction)rightRobotTouchDown;

-(IBAction) camRelease;
-(IBAction) robotRelease;

-(void) myTimerMethod;


@end
