//
//  CHADAppDelegate.m
//  CHAD
//
//  Created by Robin Mahony on 11-02-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*	
 Revision History: 
 
 Date     Author       Version   Changes
 ======== ============ ========= ================================================================
 02/18/11 Robin Mahony 1.0		Initial version.
 
 ======== ============ ========= ================================================================
 
 */

#import "CHADAppDelegate.h"
#import "chadViewController.h"

@implementation CHADAppDelegate

@synthesize window;
@synthesize	viewController;


#pragma mark -
#pragma mark Application lifecycle

- (void)applicationDidFinishLaunching:(UIApplication *)application {      
    
    // Override point for customization after application launch.
	//Hide the status bar
	//[[UIApplication sharedApplication] setStatusBarHidden:YES animated:NO];
	NSLog(@"Entered app delegate");
	// Override point for customization after app launch
	/* Init the window  */
	window = 
	[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[window setUserInteractionEnabled:YES];
	
    NSLog(@"adding viewController to window");
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
        
    //return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
	NSLog(@"applicationWillResignActive");
	[viewController setDisconnectFlag];	
	[viewController backToMenuView];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
		NSLog(@"application did enter background - CHADAppDelegate");
	//[viewController.activeView disconnectSocket];
		
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
	
	NSLog(@"application did enter foreground - CHADAppDelegate");
	//[viewController.activeView connectToSocket];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
	//[viewController backToMenuView];
	NSLog(@"ApplicationDidBecomeActive");
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
			NSLog(@"application will terminate");

}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	[viewController release];
    [window release];
    [super dealloc];
}


@end
