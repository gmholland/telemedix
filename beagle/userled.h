/* This header file contains functions to utilize the user LEDs on the
 * BeagleBoard-xM.  */
#ifndef USERLED_H
#define USERLED_H

void ledInit(int ledNum);
/* Sets trigger for user LED 0 or 1 (specified by ledNum) to "none"
 * allowing its status to be changed by other functions in this library.
 * As a result turns off the LED specified by ledNum.
 * Parameters: int ledNum - specifies the LED to initilize */

void ledHeartbeat(int ledNum);
/* Sets trigger for user LED 0 or 1 (specified by ledNum) to "heartbeat"
 * causing LED to blink with heartbeat pattern.
 * Parameters: int ledNum - specifies the LED to trigger */

void ledOn(int ledNum);
/* Turns on user LED 0 or 1 (specified by ledNum).
 * Parameters: int ledNum - specifies the LED to turn on */

void ledOff(int ledNum);
/* Turns on user LED 0 or 1 (specified by ledNum).
 * Parameters: int ledNum - specifies the LED to turn off */

void ledError(int errNo=1);
/* Turns on appropriate LED given the error type (specified by errNo)
 * Currently ledError puts heartbeat pattern on user 0 LED */

#endif //USERLED_H

