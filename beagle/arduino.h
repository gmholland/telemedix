#ifndef ARDUINO_H
#define ARDUINO_H

//function prototypes
int openPort();
int configPort(int fd);
int arduino(int fd, const char data[]); // send data to arduino

#endif //ARDUINO_H