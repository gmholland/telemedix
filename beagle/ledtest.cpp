#include <iostream>
#include "userled.h"
using namespace std;

int main()
{
    cout << "Testing each function in userled.h\n";
    cout << "\nTesting ledInit:\n";
    cout << "Output of ledInit(0):\n";
    ledInit(0);
    sleep(5);
    cout << "Output of ledInit(1):\n";
    ledInit(1);
    sleep(5);

    cout << "\nTesting ledHeartbeat\n";
    cout << "Output of ledHeartbeat(0):\n";
    ledHeartbeat(0);
    sleep(5);
    cout << "Output of ledHeartbeat(1):\n";
    ledHeartbeat(1);
    sleep(5);

    cout << "\nTesting ledOn\n";
    cout << "Output of ledOn(0):\n";
    ledOn(0);
    sleep(5);
    cout << "Output of ledOn(1):\n";
    ledOn(1);
    sleep(5);

    cout << "\nTesting ledOff\n";
    cout << "Output of ledOff(0):\n";
    ledOff(0);
    sleep(5);
    cout << "Output of ledOff(1):\n";
    ledOff(1);
    sleep(5);

    cout << "\nTesting ledError\n";
    cout << "Output of ledError(0):\n";
    ledError(0);

    cout << "Done testing\n";
    return 0;
}

