#!/usr/bin/env python
import struct
inputDevice = "/dev/input/event4" # user button mapped as event4
inputEventFormat = 'iihhi'
inputEventSize = 16

file = open(inputDevice, "rb")
event = file.read(inputEventSize)
while event:
    (time1, time2, type, code, value) = struct.unpack(inputEventFormat, event)
    if type == 1 and code == 276 and value == 1:
        print "User button pressed!"
    event = file.read(inputEventSize)
file.close()
