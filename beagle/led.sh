#!/bin/bash
# NOTE: this script must be run as root

# Turn heartbeat trigger off for usr0 LED (D6) 
echo none > /sys/class/leds/beagleboard\:\:usr0/trigger
