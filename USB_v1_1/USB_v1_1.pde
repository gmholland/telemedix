#include <Servo.h>  //inlude servo library

Servo xServo; //servo object for panning
Servo yServo; //servo object for tilting
int servoSpd = 5; // incremental change of servo position

//servo limits
int xServoMin = 0 + servoSpd;
int xServoMax = 180 - servoSpd;
int yServoMin = 55 + servoSpd;
int yServoMax = 180 - servoSpd;
int xServoCentre = (xServoMin + xServoMax)/2;
int yServoCentre = (yServoMin + yServoMax)/2;

//servo position variables
int xPos = xServoCentre;    // variable to store the servo position 
int yPos = yServoCentre;    // variable to store the servo position 
int servoDelay = 10; //time in milliseconds to wait for servo to move

//robot and camera command variables
int r = 0;
int c = 0;
  
void setup() {
  delay(3000);
  Serial.begin(9600);  
  
  //setup output pins
  pinMode(22, OUTPUT); //y-direction
  pinMode(23, OUTPUT); //y
  pinMode(24, OUTPUT); //x direction
  pinMode(25, OUTPUT); //x
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);
  pinMode(28, OUTPUT);
  pinMode(29, OUTPUT);

  //setup servo motors to PWM pins
  xServo.attach(12);  //attach x servo to pin 12
  yServo.attach(13);  //attach y servo to pin 13 
  
  xServo.write(xServoCentre);
  yServo.write(yServoCentre);
}

int getCommand()
{
  int a, data = 0;
  char s[3];
  char c;
  int i = 0;
    while(i<3)
    {
      if (Serial.available())
      {
        s[i] = char(Serial.read());
        Serial.println(s[i]);
        if(char(Serial.peek()) == '#')
        {
          c = char(Serial.read());
          break;
        }
        i++;
      }
    }
    return atoi(s);
}

void loop()
{
  int data = getCommand();
  //PORTA = data;
  Serial.println("main");
  Serial.println(data);
  
  r = getUpper(data);
  c = getLower(data);
  
  Serial.println("upper");
  Serial.println(r);
  
  Serial.println("lower");
  Serial.println(c);
 
  moveRobot(r);
 // moveCamera(c);*/
}
int getLower(int d)
{
  int l = 0;
  l = 8*bitRead(d, 3) + 4*bitRead(d, 2) + 2*bitRead(d, 1) + bitRead(d, 0);
  return l;
}

int getUpper(int d)
{
  int l = 0;
  l = 8*bitRead(d, 7) + 4*bitRead(d, 6) + 2*bitRead(d, 5) + bitRead(d, 4);
  return l;
}


void moveRobot(int r)
{
  switch (r)
  {
    case 8: //1000 move left
    PORTA = B00001100;
    break;

    case 4: //0100 move right
    PORTA = B00001000;
    break;

    case 2: //0010 move forwards
    PORTA = B00000010;
    break;

    case 1: //0001 move backwards
    PORTA = B00000011;
    break;

    default: //nothing matches
    PORTA = B00000000;  //do nothing
    break;
  }
}
/*
void moveCamera(int c)
{
  switch (c)
    {
      case 15: //1111 center camera
        xPos = xServoCentre;
        yPos = yServoCentre;
        xServo.write(xPos);
        yServo.write(yPos);
        break;
            
      case 8: //1000 look left
        if (xPos > xServoMin)
        {
          xPos = xPos - servoSpd;
          xServo.write(xPos);
          delay(servoDelay); 
        }
        else
        {
          xServo.write(xServoMin);       
          delay(servoDelay);  
        }
        break;
      
      case 4: //0100 look right
        if (xPos < xServoMax)
        {
          xPos = xPos + servoSpd;
          xServo.write(xPos);
          delay(servoDelay); 
        }
        else
        {
          xServo.write(xServoMax);       
          delay(servoDelay);  
        }
        break;
      
      case 2: //0010 look up
        if (yPos < yServoMax)
        {
          yPos = yPos + servoSpd;
          yServo.write(yPos);
          delay(servoDelay); 
        }
        else
        {
          yServo.write(yServoMax);       
          delay(servoDelay);  
        }
        break;
      
      case 1: //0001 look down
        if (yPos > yServoMin)
        {
          yPos = yPos - servoSpd;
          yServo.write(yPos);
          delay(servoDelay); 
        }
        else
        {
          yServo.write(yServoMin);       
          delay(servoDelay);  
        }
        break;
    }  
}*/
