/* 
 * File:   main.cpp
 * Author: adrian
 *
 * Created on April 9, 2011, 4:36 PM
 */

#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

using namespace std;

int fd;//file descriptor for port

//function prototypes
int openPort();
int configPort();
int arduino(char data[4]);

int main()
{
	int port, cfg, flg;
	char data[4];

	port = openPort();
	cfg = configPort();
    sleep(2);

	if (port < 0 || cfg < 0)
	{
        close(fd);
	}
	else
	{
        while(1)
        {
            cout << "Type data, or q to quit\n";
            cin >> data;
            
            if(data[0] == 'q')
                break;
            
            int flg = arduino(data);
            
            if(flg < 0)
            {
                cerr << "Communication breakdown! \n";
                //return -1;
            }
            
            //clear data
            data[0] = '\0';
            data[1] = '\0';
            data[2] = '\0';
            data[3] = '\0';
        }
	}
	close(fd);
    cout << "Program ended, device closed\n";
	return 0;
}

int openPort()
{
    fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)
    {
        cerr << "Unable to open port\n";
        return -1;
    }
    else
    {
        fcntl(fd, F_SETFL, 0);
        cout << "Port opened succesfully\n";
    }
    return 0;
}

int configPort()
{
    struct termios options;
    //Get the current options for the port...
    tcgetattr(fd, &options);
    //set baudrate
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    //set parity checking
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    int n = tcsetattr(fd, TCSAFLUSH, &options);
    if (n < 0)
    {
        cerr << "Port configuration fail\n";
        return n;
    }
    else
        return 0;
}

int arduino(char data[4])
{       
    int wr, rd;
    char reply;
    
    cout << "Write: " << data << "\n";
    
    wr = write(fd, data, 4);
                        
    if (wr < 1)
    {
        cerr << "Write failed\n";
        return -1;
    }

    rd = read(fd, &reply, 1);
            
    while (rd < 1)
    {
        cerr << "Read failed\n";
        rd = read(fd, &reply, 1);
    }
    if (reply == 'y')
        return 0;
    else
        return -1;  
}
